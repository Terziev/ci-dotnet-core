using Pets.Pets;
using System;
using Xunit;

namespace PetsTests
{
    public class PetTests
    {
        [Fact]
        public void DogTalkToOwnerTest()
        {
            string expected = "Woof!";
            string actual = new Dog().TalkToOwner();

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CatTalkToOwner()
        {
            string expected = "Meow!";
            string actual = new Cat().TalkToOwner();

            Assert.Equal(expected, actual);
        }
    }
}
