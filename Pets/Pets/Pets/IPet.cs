﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pets.Pets
{
    interface IPet
    {
       string TalkToOwner();
    }
}
