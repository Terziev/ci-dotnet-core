﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pets.Pets
{
    public class Dog : IPet
    {
        public string TalkToOwner() => "Woof!";
    }
}
