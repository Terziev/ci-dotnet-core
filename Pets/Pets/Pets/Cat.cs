﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pets.Pets
{
    public class Cat : IPet
    {
        public string TalkToOwner() => "Meow!";
    }
}
