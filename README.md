# Information

This is a sample .net core project using xUnit for testing.

The purpose of the project is to facilitate the usage of the build-in GitLab runners.

For more information about the GitLab CI/CD configuration refer to the .gitlab-ci.yml file.

## Steps

In order to run the project and/or run the tests, execute the following commands:

1) cd Pets
2) dotnet build
3) dotnet test